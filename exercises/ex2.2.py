#!/usr/bin/env python3
import sys
import os
import random
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import DFA, NFA
from tests.utils import get_random_input

nfa = NFA(
    states = {'s', 't', 'u', 'v'},
    alphabet = {'a', 'b'},
    transitions = {
        's': {'a': {'s', 't'},  'b': {'s'}},
        't': {'a': {'u'},       'b': {}},
        'u': {'a': {},          'b': {'v'}},
        'v': {'a': {},          'b': {}}
    },
    start_states = {'s'},
    final_states = {'v'}
)

# convert NFA to DFA
dfa = nfa.to_dfa()
print('Converted DFA from NFA')
print(dfa)

input() # pause execution
# remove inaccessible states
dfa.remove_inaccessible_states()
print('DFA after removing inaccessible states')
print(dfa)


random_inputs = get_random_input(1000, ['a', 'b'])
for input_string in random_inputs:
    assert nfa.accepts(input_string) == dfa.accepts(input_string)
