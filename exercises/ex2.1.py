#!/usr/bin/env python3
import sys
import os
import random
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import DFA, NFA
from tests.utils import get_random_input


nfa = NFA(
    states = {1, 2},
    alphabet = {'a', 'b'},
    transitions = {
        1: {'a': {1, 2}, 'b': set()},
        2: {'a': set(),  'b': {1, 2}},
    },
    start_states = {1},
    final_states = {1}
)

# convert NFA to DFA
dfa = nfa.to_dfa()
print('Converted DFA from NFA')
print(dfa)


T# remove inaccessible states
dfa.remove_inaccessible_states()
print('DFA after removing inaccessible states')
print(dfa)


# "validate" dfa
random_inputs = get_random_input(100, ['a', 'b'])
for input_string in random_inputs:
    assert nfa.accepts(input_string) == dfa.accepts(input_string)

