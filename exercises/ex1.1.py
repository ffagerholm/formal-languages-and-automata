#!/usr/bin/env python3
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import DFA


dfa = DFA(
    states = {'1', '2', '3', '4', '5', '6', '7', '8'}, 
    alphabet = {'a', 'b'}, 
    transitions = {
        '1': {'a': '1', 'b': '2'},
        '2': {'a': '3', 'b': '6'},
        '3': {'a': '4', 'b': '5'},
        '4': {'a': '1', 'b': '2'},
        '5': {'a': '3', 'b': '6'},
        '6': {'a': '7', 'b': '8'},
        '7': {'a': '4', 'b': '5'},
        '8': {'a': '7', 'b': '8'}
    }, 
    start_state = '1', 
    final_states = {'4', '5', '7', '8'}
)


print('\nInput:', '"baa"')
print('Accepts:', dfa.accepts('baa', trace=True))

print('\nInput:', '"abab"')
print('Accepts:', dfa.accepts('abab', trace=True))

print('\nInput:', '"abab"')
print('Accepts:', dfa.accepts('abab', trace=True))

print('\nInput:', '"abbbaa"')
print('Accepts:', dfa.accepts('abbbaa', trace=True))
