#!/usr/bin/env python3
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import DFA


# 2 a)
# Automaton that accepts string with '481' as a substring
dfa_2a = DFA(
    states = {'s', 'p', 'q', 'r'}, 
    alphabet = {'4', '8', '1'}, 
    transitions = {
        's': {'4': 'p', '8': 's', '1': 's'},
        'p': {'4': 'p', '8': 'q', '1': 's'},
        'q': {'4': 'p', '8': 's', '1': 'r'},
        'r': {'4': 'r', '8': 'r', '1': 'r'}
    }, 
    start_state = 's', 
    final_states = {'r'}
)

print(dfa_2a)

input_2a = ['481', '4481', '8411', '18481814']

for input_string in input_2a:
    input() # pause execution
    print('Input:', input_string)
    print('Accepts:', dfa_2a.accepts(input_string, trace=True), '\n')
    
