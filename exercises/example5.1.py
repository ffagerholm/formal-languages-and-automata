#!/usr/bin/env python3
import sys
import os
import random
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import NFA

# Example 5.1 from Automata and Computability, D Kozen
nfa = NFA(
    states = {'p', 'q', 'r'},
    alphabet = {'0', '1'},
    transitions = {
        'p': {'0': {'p'}, '1': {'p', 'q'}},
        'q': {'0': {'r'}, '1': {'r'}},
        'r': {'0': set(), '1': set()}
    },
    start_states = {'p'},
    final_states = {'r'}
)

dfa = nfa.to_dfa()
print(dfa)
dfa.remove_inaccessible_states()
print(dfa)