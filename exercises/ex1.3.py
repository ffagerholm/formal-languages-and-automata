#!/usr/bin/env python3

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from automata import DFA


dfa1 = DFA(
    states = {'1', '2'}, 
    alphabet = {'a', 'b'}, 
    transitions = {
        '1': {'a': '1', 'b': '2'},
        '2': {'a': '2', 'b': '1'}
    }, 
    start_state = '1', 
    final_states = {'2'}
)

dfa2 = DFA(
    states = {'1', '2', '3'}, 
    alphabet = {'a', 'b'}, 
    transitions = {
        '1': {'a': '2', 'b': '3'},
        '2': {'a': '3', 'b': '2'},
        '3': {'a': '1', 'b': '2'}
    }, 
    start_state = '1', 
    final_states = {'3'}
)

print('DFA 1')
print('\nInput:', '"aba"')
print('Accepts:', dfa1.accepts('aba', trace=True))
print('\nInput:', '"baaab"')
print('Accepts:', dfa1.accepts('baaab', trace=True))


print('\nDFA 2')
print('\nInput:', '"b"')
print('Accepts:', dfa2.accepts('b', trace=True))

print('\nInput:', '"aa"')
print('Accepts:', dfa2.accepts('aa', trace=True))

print('\nInput:', '"bba"')
print('Accepts:', dfa2.accepts('bba', trace=True))


product_dfa = dfa1.product(dfa2)
print('\nProduct DFA')

print('\nInput:', '"b"')
print('Accepts:', product_dfa.accepts('b', trace=True))

print('\nInput:', '"bba"')
print('Accepts:', product_dfa.accepts('bba', trace=True))

print('\nInput:', '"bbaba"')
print('Accepts:', product_dfa.accepts('bbaba', trace=True))
