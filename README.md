# Formal languages and automata

Implementations of finite automata, grammars and turing machines. 

Initially made for illustrating automata theory when working as an assistant on a course about formal languages and automata. 

Theory and some of the examples are from the book [Automata and Computability](https://www.springer.com/us/book/9780387949079) by Dexter C. Kozen.

Implementations of the automata are inspired by [this repository](https://github.com/caleb531/automata). 

The code for generating random sentences from a context-free grammar are inspired by [this blogpost](https://eli.thegreenplace.net/2010/01/28/generating-random-sentences-from-a-context-free-grammar).


## To do
- [] Restructure repository, unify Regex and automata directories.
- [] Add explanation and examples to readme.
- [] Tests
