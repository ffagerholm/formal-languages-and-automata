"""
Example 19.2 from Automata and Computability, Dexter C. Kozen

Context free grammar for the language of palindormes over the
alphabet {a, b}
"""
from __future__ import print_function
from context_free_grammar import CFG


cfg = CFG(
    nonterminals = {'S'},
    terminals = {'a', 'b'},
    productions = {
        'S': [(''), ('a'), ('b'), ('a', 'S', 'a'), ('b', 'S', 'b')],
    },
    start_symbol = 'S'
)

for sent in cfg.generate(depth=3):
    print(''.join(sent))

