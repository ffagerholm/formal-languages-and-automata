from __future__ import print_function
from datetime import datetime
from itertools import chain
from midi import load_midi_dict, notes_to_midi
from context_free_grammar import CFG


# A minor pentatonic scale
productions = {
    'S': [
        # Recursive rule (start symbols followed by our notes)
        ('S', 'Am', 'Am', 'Am', 'Am',), 
        # Base case (sequence of four notes)
        ('Am', 'Am', 'Am', 'Am',),    
    ],
    # possible note in the scale
    'Am': [('A3',), ('C3',), ('D3',), ('E3',), ('G3',), ('A4',),],
}

nonterminals = frozenset(productions.keys())
terminals = frozenset(list(chain.from_iterable(productions['Am'])))

cfg = CFG(
    nonterminals = nonterminals,
    terminals = terminals,
    productions = productions,
    start_symbol = 'S'
)


if __name__ == '__main__':
    midi_dict = load_midi_dict() 

    sequence = cfg.generate_random_sentece(start_symbols=['S', 'S', 'S', 'S'], 
                                           cfactor=0.5)
    print(', '.join(sequence))
    # get midi values for notes
    midi_notes = [midi_dict[note] for note in sequence]

    midi_file = notes_to_midi(midi_notes, time=256)
    # write midi to file
    dt = datetime.now()
    file_name = 'generated{:{dfmt} {tfmt}}.mid'.format(dt, dfmt='%Y-%m-%d', tfmt='%H.%M.%m')
    midi_file.save(file_name)
