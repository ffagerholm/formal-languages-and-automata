import os
import sys
import json
import numpy as np
from scipy.signal import savgol_filter, medfilt
from scipy.signal import butter, lfilter, freqz
from scipy.io.wavfile import write

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

def load_freq_dict():
    path = os.path.join(os.path.dirname(__file__), 'notes_frequencies.json')
    with open(path, 'r') as infile:
        data = json.loads(infile.read())

    return data


def butter_lowpass(cutoff, fs, order=5):
    """Get coefficients for a Butterworth filter.
    Args:
        cutoff (float): Cutoff frequency.
        fs (int): Fampling rate
        order (int): Filter order.
    Returns: 
        b, a (pair): Numerator (b) and denominator (a) polynomials 
            of the IIR filter.
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    """Apply a lowpass filter.
    Args:
        data (array): Array containing the data to be filtered.
        cutoff (float): Cutoff frequency.
        fs (int): Sampling rate.
        order (int): filter order.
    Returns:
        y (array): Filtered data.
    """
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


def make_tone(freq, duration=0.18, amp=1, rate=44100):
    """Create a tone.
    A note is a array of integer values encoding the waveform of the audio.
    
    Args:
        freq (float): Frequency of the tone.
        duration (float): duration in seconds.
        amp (float): amplitude of the tone.
        rate (int): sample rate.
    """
    t = np.linspace(0, duration, duration * rate)
    data = np.sin(2*np.pi*freq*t)*amp
    return data.astype(np.int16) # two byte integers


def make_tones_dict():
    """Get a dictionary of note names mapped to their array representation.
    """
    note_freqs = load_freq_dict()
    tones = dict()
    for note, freq in note_freqs.items():
        tones[note] = make_tone(freq)

    return tones


if __name__ == '__main__':
    np.random.seed(0)
    # get map of note names to frequencies
    tones = make_tones_dict()
    # create a sequence of random notes 
    sequence = [tones[key] for key in np.random.choice(tones.keys(), size=(16,))]
    # apply filter 
    data = medfilt(np.concatenate(tuple(sequence), axis=0))
    
    filtered_data = butter_lowpass_filter(data, cutoff=300.0, rate, order=8)

    write('song.wav', 44100, filtered_data)
