import sys
import os
import json
from mido import Message, MidiFile, MidiTrack

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))


def load_midi_dict():
    path = os.path.join(os.path.dirname(__file__), 'notes_midi.json')
    with open(path, 'r') as infile:
        data = json.loads(infile.read())

    return data


def notes_to_midi(notes, program=0, time=512):
    mid = MidiFile(type=0)
    track = MidiTrack()
    mid.tracks.append(track)

    track.append(Message('program_change', program=program, time=0))

    for k in notes:
        track.append(Message('note_on', note=k, velocity=127, time=time))

    track.append(Message('note_on', note=k, velocity=127, time=time*8)) 

    return mid



if __name__ == "__main__":
    notes_to_midi()