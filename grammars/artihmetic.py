from __future__ import print_function

from context_free_grammar import CFG

productions = {
    'S': [['EXPR']],
    'EXPR': [['TERM'], ['TERM', '+', 'EXPR'], ['TERM', '-', 'EXPR']],
    'TERM': [['FACTOR', '*', 'TERM'], ['FACTOR', '/', 'TERM'], ['FACTOR']], 
    'FACTOR': [['ID'], ['NUM'], ['EXPR']],
    'ID': [['x'], ['y'], ['z'], ['w']],
    'NUM': [['0'], ['1'], ['2'], ['3'], ['4'],
    ['5'], ['6'], ['7'], ['8'], ['9']]
}

nonterminals = {'S', 'EXPR', 'TERM', 'FACTOR', 'ID', 'NUM'}
terminals = {
    '+', '-', '*', '/', 
    'x', 'y', 'z', 'w',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
}

cfg = CFG(
    nonterminals = nonterminals,
    terminals = terminals,
    productions = productions,
    start_symbol = 'S'
)

for _ in range(10):
    print(''.join(cfg.generate_random_sentece()))