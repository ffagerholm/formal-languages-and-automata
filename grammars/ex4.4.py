from __future__ import print_function
from context_free_grammar import CFG

if __name__ == "__main__":
    cfg = CFG(
        nonterminals = {'S', 'L', 'R'},
        terminals = {'<div>', '</div>'},
        productions = {
            'S': [('L', 'R'), ('L', 'S', 'R'), ('S', 'S')],
            'L': [('<div>',)],
            'R': [('</div>',)]
        },
        start_symbol = 'S'
    )

    for sent in cfg.generate(depth=5):
        print(''.join(sent))

    print(''.join(cfg.generate_random_sentece()))
