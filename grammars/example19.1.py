"""
Example 19.1 from Automata and Computability, Dexter C. Kozen

Context free grammar for the language A = {a^nb^n | n >= 0}
"""
from __future__ import print_function
from context_free_grammar import CFG

cfg = CFG(
    nonterminals = {'S'},
    terminals = {'a', 'b'},
    productions = {
        'S': [(''), ('a', 'S', 'b')],
    },
    start_symbol = 'S'
)

for sent in cfg.generate(depth=5):
    print(''.join(sent))

