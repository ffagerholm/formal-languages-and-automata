from __future__ import print_function
from nltk.parse.generate import generate
from nltk import CFG

grammar_str = """
              S -> S S | L S R | L R
              L -> '['
              R -> ']'
              """

grammar = CFG.fromstring(grammar_str)

words = generate(grammar, depth=5)
words = sorted(words, key=len)

for w in words:
    print(''.join(w))

