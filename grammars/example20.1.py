"""
Example 19.2 from Automata and Computability, Dexter C. Kozen

Context free grammar for the language PAREN of balanced parentheses.
A string x over the alphabet {[, ]} is balanced if 
    1. x contains as many [ as ]
    2. any prefix of x has a greater or equal number of [ than ]
"""
from __future__ import print_function
from context_free_grammar import CFG

cfg = CFG(
    nonterminals = {'S', 'L', 'R'},
    terminals = {'[', ']'},
    productions = {
        'S': [('[', ']'), ('[', 'S', ']'), ('S', 'S')],
    },
    start_symbol = 'S'
)

for sent in cfg.generate(depth=4):
    print(''.join(sent))
