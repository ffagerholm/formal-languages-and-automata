"""
Example with a context free grammar for "natural language".

Loads a grammar specification from a file and prints some
example sentences.

Example grammar from 
https://github.com/aparrish/rwet-examples/blob/master/contextfree/test_grammar.json
"""
from __future__ import print_function
import json

from context_free_grammar import CFG

"""
with open('test_grammar.json') as file_object:
    productions = json.loads(file_object.read())
"""
productions = {
	"PP": [["Prep", "NP"]], 
	"Vintr": [["coughs"], ["daydreams"], ["whines"], ["slobbers"], ["vocalizes"], ["sneezes"]], 
	"Det": [["this"], ["that"], ["the"]], 
	"Vtrans": [["computes"], ["examines"], ["foregrounds"], ["prefers"], ["interprets"], ["spins"]], 
	"N": [["amoeba"], ["dichotomy"], ["seagull"], ["trombone"], ["corsage"], ["restaurant"], ["suburb"]], 
	"VP": [["Vtrans", "NP"], ["Vintr"]], 
	"S": [["NP", "VP"], ["Interj", "NP", "VP"]], 
	"Interj": [["oh,"], ["my,"], ["wow,"], ["damn,"]], 
	"NP": [["Det", "N"], ["Det", "N", "that", "VP"], ["Det", "Adj", "N"], ["Det", "N", "PP"]], 
	"Adj": [["bald"], ["smug"], ["important"], ["tame"], ["overstaffed"], ["luxurious"], ["blue"]], 
	"Prep": [["in"], ["on"], ["over"], ["against"]]
}

nonterminals = list(productions.keys())


cfg = CFG(
    nonterminals = nonterminals,
    terminals = {},
    productions = productions,
    start_symbol = 'S'
)

for sent in cfg.generate(depth=4):
    print(' '.join(sent))

print(' '.join(cfg.generate_random_sentece()))