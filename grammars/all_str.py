from __future__ import print_function
from context_free_grammar import CFG

cfg = CFG(
    nonterminals = {'S'},
    terminals = {'a', 'b'},
    productions = {
        'S': [('',), ('a', 'S', 'b', 'b')],
    },
    start_symbol = 'S'
)

sents = sorted(map(lambda x: ''.join(x) + '\n', cfg.generate(depth=20)), key=len)

with open('/home/fredrik/input.txt', 'w') as file_object:
    file_object.writelines(sents)
