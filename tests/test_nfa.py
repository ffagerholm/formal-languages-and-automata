import unittest

from automata import NFA
from .utils import *


class TestNFA(unittest.TestCase):
    def setUp(self):
        self.nfa1 = NFA(
            states = {1, 2},
            alphabet = ['a', 'b'],
            transitions = {
                1: {'a': {1, 2}, 'b': set()},
                2: {'a': set(),  'b': {1, 2}},
            },
            start_states = {1},
            final_states = {1}
        )

        self.dfa1 = self.nfa1.to_dfa()

    
    def test_nfa1_accepts(self):
        inputs = get_random_input(1000, self.dfa1.alphabet, min_length=1) 
        for x in inputs:
            # string starts with 'a'
            starts_with_a = (x[0] == 'a')
            self.assertEqual(self.nfa1.accepts(x), starts_with_a)


    def test_subset_construction(self):
        inputs = get_random_input(1000, self.dfa1.alphabet, min_length=1) 
        for x in inputs:
            # starts with 'a'
            starts_with_a = (x[0] == 'a')
            self.assertEqual(self.dfa1.accepts(x), starts_with_a)
            # test that the same language is accepted
            self.assertEqual(self.dfa1.accepts(x), self.nfa1.accepts(x))


if __name__ == '__main__':
    unittest.main()