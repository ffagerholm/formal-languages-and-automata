import random


def get_random_string(length, alphabet):
    return ''.join([random.choice(alphabet) for _ in range(length)])


def get_random_input(number, alphabet, min_length=0, max_length=50):
    random_lengths = [random.randint(min_length, max_length) 
                            for _ in range(number)]
    # generate random input
    random_inputs = [get_random_string(length, alphabet) 
                            for length in random_lengths]
    return random_inputs
