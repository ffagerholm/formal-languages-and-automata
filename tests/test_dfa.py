import unittest

from automata import DFA, UndefinedSymbolException, \
                     UndefinedTransitionException
from .utils import *


class TestDFA(unittest.TestCase):
    def setUp(self):
        # automaton that accepts strings that start with 'a'
        self.dfa1 = DFA(
            states = ['s', 'p', 'q'], 
            alphabet = ['a', 'b'], 
            transitions = {
                's': {'a': 'p', 'b': 'q'},
                'p': {'a': 'p', 'b': 'p'},
                'q': {'a': 'q', 'b': 'q'}, # garbage
            }, 
            start_state = 's', 
            final_states = ['p']
        )

        self.dfa2 = DFA(
            states = ['s', 'p'], 
            alphabet = ['a', 'b'], 
            transitions = {
                's': {'a': 's', 'b': 'p'},
                'p': {'a': 'p', 'b': 's'},
            }, 
            start_state = 's', 
            final_states = ['s']
        )

        self.dfa_prod = self.dfa1.product(self.dfa2)

        # faulty automata
        self.dfa3 = DFA(
            states = ['s'], 
            alphabet = ['a', 'b'], 
            transitions = {
                's': {'a': 's'},
            }, 
            start_state = 's', 
            final_states = ['s']
        )
    
    def test_dfa1_accepts(self):
        inputs = get_random_input(1000, self.dfa1.alphabet, min_length=1) 
        for x in inputs:
            # string starts with 'a'
            starts_with_a = (x[0] == 'a')
            self.assertEqual(self.dfa1.accepts(x), starts_with_a)

    def test_dfa2_accepts(self):
        inputs = get_random_input(1000, self.dfa2.alphabet) 
        for x in inputs:
            # even number of 'b':s
            is_even = (x.count('b') % 2 == 0)
            self.assertEqual(self.dfa2.accepts(x), is_even)

    def test_product(self):
        inputs = get_random_input(1000, self.dfa1.alphabet, min_length=1) 
        for x in inputs:
            # even number of 'b':s
            is_even = (x.count('b') % 2 == 0)
            # starts with 'a'
            starts_with_a = (x[0] == 'a')
            # test that intersection language is accepted
            self.assertEqual(self.dfa_prod.accepts(x), is_even and starts_with_a)

    def test_undef_symbol(self):
        with self.assertRaises(UndefinedSymbolException):
            self.dfa1.accepts('c')

    def test_undef_transition(self):
        with self.assertRaises(UndefinedTransitionException):
            self.dfa3.accepts('ab')


if __name__ == '__main__':
    unittest.main()