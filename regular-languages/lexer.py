

class Token():
    """
    """
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __repr__(self):
        return "{}:{}".format(self.name, self.value)


class Lexer():
    """
    """
    def __init__(self, pattern):
        self.source = pattern
        self.symbols = {'(': 'LEFT_PAREN', 
                        ')': 'RIGHT_PAREN', 
                        '*': 'STAR', 
                        '+': 'ALT', 
                        '\x08': 'CONCAT'}
        self.current = 0
        self.length = len(self.source)
       
    def get_token(self): 
        if self.current < self.length:
            c = self.source[self.current]
            self.current += 1
            if c not in self.symbols.keys(): # CHAR
                token = Token('CHAR', c)
            else:
                token = Token(self.symbols[c], c)
            return token
        else:
            return Token('NONE', '')