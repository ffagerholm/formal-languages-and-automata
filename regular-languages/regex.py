from lexer import Lexer 
from parser import Parser
from handler import Handler


class Regex():
    """
    """
    def __init__(self, expression):
        lexer = Lexer(expression)
        parser = Parser(lexer)
        self.tokens = parser.parse()

    def to_nfa(self):
        handler = Handler()
        nfa_stack = []
    
        for t in self.tokens:
            handler.handlers[t.name](t, nfa_stack)
    
        assert len(nfa_stack) == 1
        return nfa_stack.pop() 


if __name__ == '__main__':
    from regex import Regex
    regex = Regex('a+b')
    nfa = regex.to_nfa()

    nfa.match('ab')