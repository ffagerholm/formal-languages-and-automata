
class ParseError(Exception):
    def __init__(self, message):
        super(ParseError, self).__init__(message)
