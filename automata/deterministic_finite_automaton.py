#!/usr/bin/env python3

from copy import copy, deepcopy
from itertools import product

from automata.automaton import FiniteAutomaton
from automata.exceptions import UndefinedTransitionException, UndefinedSymbolException


class DFA(FiniteAutomaton):
    """
    Deterministic finite automaton.
    Args:
        states (set): set of states
        alphabet (set): set of symbols
        transitions (dict): dictionary of dictionaries defining
            the transition function. The 'outer' keys are states
            and the 'inner' keys are symbols. For a state 'q' and a
            symbol 'a': transitions['q']['a'] should give the state
            that the automaton moves to from state 'q' on input 'a'.
        start_state (str): start state of automaton.
        final_state (set): set of accepting states.

    NOTE individual states and symbols should be immutable objects
    """
    def __init__(self, states, alphabet, transitions,
                       start_state, final_states):
        self.states = copy(states)
        self.alphabet = copy(alphabet)
        self.transitions = deepcopy(transitions)
        self.start_state = start_state
        self.final_states = copy(final_states)

    def accepts(self, input_string, trace=False):
        """
        Run the automaton on a string.
        Checks if the automaton stops in an accepting state
        after the input string has been processed.
        Args:
            input_string (str): string consisting of symbols from
                the alphabet.
            trace (bool): print transitions the automaton goes through.

        Returns:
            (bool): True if the string is accepted, otherwise False

        Raises:
            UndefinedSymbolException: symbol not in alphabet found in
                the input string.
            UndefinedTransitionException: no transition was defined for
                the combination of state and symbol.
        """

        current_state = self.start_state
        # go through each symbol in the input string
        for symbol in input_string:
            if symbol not in self.alphabet:
                raise UndefinedSymbolException(
                        "symbol '{}' not in alphabet".format(symbol))
            
            if trace:
                # print current state and symbol
                print((current_state, symbol), end='')

            try:
                # move to next state
                current_state = self.transitions[current_state][symbol]
            except KeyError:
                raise UndefinedTransitionException(
                    "no transition defined from state '{}' for input symbol '{}'".format(
                        current_state, symbol))
            
            if trace:
                # print new state after transition
                print(" -> ", "'{}'".format(current_state))

        # check if the state is accepting
        return current_state in self.final_states


    def remove_inaccessible_states(self):
        """
        Remove states not reachable from the start state.
        """
        visited = set()
        state_stack = [self.start_state]

        while state_stack:
            p = state_stack.pop()
            visited.add(p)
            for symbol in self.alphabet:
                q = self.transitions[p][symbol]
                if q not in visited:
                    state_stack.append(q)

        for state in self.states:
            if state not in visited:
                self.transitions.pop(state, None)

        self.states = frozenset(visited)


    def product(self, dfa):
        """
        Create the product between this and another automata.
        The product automaton accepts the language that
        is the intersection of the languages accepted by
        each automaton.
        If the two automata are defined as
        M1 = (Q1, S, d1, s1, F1)
        and  M2 = (Q2, S, d2, s2, F2)
        then the product automata M1 x M2 if defined as
        M3 = (Q3, S, d3, s3, F3)
        where
        Q3 = Q1 x Q2 = {(p, q) | p in Q1, q in Q2}
        F3 = F1 x F2 = {(p, q) | p in F1, q in F2}
        s3 = (s1, s2)
        d3((p, q), a) = (d1(p, a), d2(q, a))

        Args:
            dfa (DFA): the other DFA in the product.

        Returns:
            (DFA): product automaton.

        Raises:
            ValueError: if automatons does not have the
                same alphabet.

        """
        if not self.alphabet == dfa.alphabet:
            raise ValueError(
                'the automatons does not have the same alphabet')

        # create the states of the product automaton
        product_states = list(product(self.states, dfa.states))
        product_final_states = list(product(self.final_states, dfa.final_states))
        product_start_state = (self.start_state, dfa.start_state)

        # create transition function of the product automata
        product_transitions = dict()
        for p, q in product_states:
            product_transitions[(p, q)] = {}
            for symbol in self.alphabet:
                 product_transitions[(p, q)][symbol] = \
                    (self.transitions[p][symbol], dfa.transitions[q][symbol])

        # create a new DFA
        product_dfa = DFA(
            states = product_states,
            alphabet = self.alphabet,
            transitions = product_transitions,
            start_state = product_start_state,
            final_states = product_final_states
        )

        return product_dfa

    def __repr__(self):
        """
        Get string representation of automaton.
        Returns the transition table for the automaton.

        NOTE the size of the table does not adapt
        to the length of the state names, so the 
        output may look strange for state names longer 
        than 22 characters
        TODO make table size adaptive
        """
        repr_str = ''
        states = sorted(self.states, key=len)
        alphabet = sorted(self.alphabet)

        repr_str += '{:^25}|'.format('q')
        for s in alphabet:
            repr_str += '{:^25}|'.format(s)
        repr_str += '\n'

        for _ in range(len(alphabet) + 1):
            repr_str += '-'*25 + '+'
        repr_str += '\n'

        for q in states:
            if isinstance(q, frozenset):
                # if the state is the empty set
                if q == set():
                    q_str = '{}'
                else:
                    q_str = str(set(q))
            else:
                q_str = str(q)

            if q == self.start_state and q in self.final_states:
                repr_str += '->{:^22}F|'.format(q_str)
            elif q == self.start_state:
                repr_str += '->{:^22} |'.format(q_str)
            elif q in self.final_states:
                repr_str += '  {:^22}F|'.format(q_str)
            else:
                repr_str += '  {:^22} |'.format(q_str)

            for s in alphabet:
                p = self.transitions[q][s]
                if isinstance(p, frozenset):
                    # if the state is the empty set
                    if p == set():
                        p_str = '{}'
                    else:
                        p_str = str(set(p))
                else:
                    p_str = str(p)

                repr_str += '{:^25}|'.format(p_str)
            repr_str += '\n'

        return repr_str
