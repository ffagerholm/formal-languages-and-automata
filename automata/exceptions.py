

class UndefinedTransitionException(Exception):
    def __init__(self, message):
        super(UndefinedTransitionException, self).__init__(message)


class RejectionError(Exception):
    def __init__(self, message):
        super(RejectionError, self).__init__(message)


class UndefinedSymbolException(Exception):
    def __init__(self, message):
        super(UndefinedSymbolException, self).__init__(message)
