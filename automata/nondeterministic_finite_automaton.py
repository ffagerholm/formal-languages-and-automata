#!/usr/bin/env python3

from copy import copy, deepcopy
from itertools import combinations

from automata.automaton import FiniteAutomaton
from automata.deterministic_finite_automaton import DFA
from automata.exceptions import UndefinedTransitionException, UndefinedSymbolException


class NFA(FiniteAutomaton):
    """
    Nondeterministic finite automaton.
    Args:
        states (set): set of states
        alphabet (set): set of symbols
        transitions (dict): dictionary of dictionaries defining
            the transition function. The 'outer' keys are states
            and the 'inner' keys are sybols. For a state 'q' and a
            symbol 'a': transitions['q']['a'] should give a set of states
            that the automaton can mode to from state 'q' on input 'a'.
        start_state (str): set of start states.
        final_state (set): set of accepting states.

    NOTE individual states and symbols should be immutable objects
    """
    def __init__(self, states, alphabet, transitions,
                       start_states, final_states):
        self.states = copy(states)
        self.alphabet = copy(alphabet)
        self.transitions = deepcopy(transitions)
        self.start_states = copy(start_states)
        self.final_states = copy(final_states)

    def get_epsilon_closure(self, start_state):
        """
        Return the epsilon closure for the given state.
        The epsilon closure of a state q is the set containing q, along with
        every state that can be reached from q by following only epsilon
        transitions.
        Args:
            start_state (str): state to find the epsilon closure for
        Returns:
            closure (set): set of reachable states
        """
        stack = []
        closure = set()
        stack.append(start_state)

        while stack:
            state = stack.pop()
            if state not in closure:
                closure.add(state)
                if '' in self.transitions[state]:
                    stack.extend(self.transitions[state][''])

        return frozenset(closure)

    def get_next_states(self, current_states, symbol):
        """
        Get the set of states reachable from any of the current states
        with a transition for the symbol or epsilon transitions
        Args:
            current_states (set): set of states
            symbol (str): transition symbol
        Returns:
            next_states (set): set of reachable states states
        """
        next_states = frozenset()
        for state in current_states:
            # add states reachable with the symbol
            next_states = next_states.union(
                            self.transitions[state][symbol]
                                      )
            # add states reachable by epsilon transitions
            if '' in self.transitions[state]:
                next_states = next_states.union(
                            self.transitions[state]['']
                                          )
        return next_states

    def accepts(self, input_string, trace=False):
        """
        Run the automaton on a string.
        Checks if the set of states has at least one accepting
        state when the automaton stops.
        Args:
            input_string (str): string consisting of symbols from
                the alphabet.
            trace (bool): print states automaton goes through.

        Returns:
            (bool): True if the string is accepted, otherwise False

        Raises:
            UndefinedSymbolException: symbol not in alphabet found in
                the input string.
            UndefinedTransitionException: no transition was defined for
                the combination of state and symbol.
        """
        try:
            # hold track of possible states
            current_states = set()
            # find start states and add them to current states
            for state in self.start_states:
                reachable_states = self.get_epsilon_closure(state)
                current_states = current_states.union(reachable_states)

            # parse input string
            for symbol in input_string:
                if symbol not in self.alphabet:
                    raise UndefinedSymbolException(
                            'symbol "{}" not in alphabet'.format(symbol))
                if trace:
                    # print current state and symbol
                    print((current_states, symbol), end='')
                current_states = self.get_next_states(
                                        current_states, symbol)
                if trace:
                    print(' -> ', current_states)

            if current_states.intersection(self.final_states):
                return True
            else:
                return False

        except KeyError as key_error:
            raise UndefinedTransitionException(
                'no transitions defined from states "{}" for input symbol "{}"'.format(
                    current_states, symbol
                )
            )

    def to_dfa(self):
        """
        Transform the NFA to a DFA by subset construction.
        Returns
            dfa (DFA): deterministic automaton accepting the
                the same language as this automaton.
        """
        # construct the set of states for the DFA
        dfa_states = set()
        # the states are all possible subsets of states of the NFA
        # create all subsets of size k = 0, 1, 2, ..., |Q_NFA|
        for k in range(len(self.states) + 1):
            # subsets of size k are the same as combinations of size
            # k from elements of Q_NFA
            # NOTE combinations returns tuples
            for subset in combinations(self.states, k):
                # convert to set
                state = frozenset(subset)
                dfa_states.add(state)

        # convert to immutable set
        dfa_states = frozenset(dfa_states)

        # construct transitions
        dfa_transitions = dict()
        for state in dfa_states:
            dfa_transitions[state] = dict()
            for symbol in self.alphabet:
                next_state = self.get_next_states(set(state), symbol)
                dfa_transitions[state][symbol] = next_state

        dfa_start_state = frozenset(self.start_states)

        # construct the set of final states
        dfa_final_states = set()
        for state in dfa_states:
            # check if the intersection is empty
            if state.intersection(self.final_states) != set():
                dfa_final_states.add(state)

        dfa_final_states = frozenset(dfa_final_states)

        dfa = DFA(
            states = dfa_states,
            alphabet = self.alphabet,
            transitions = dfa_transitions,
            start_state = dfa_start_state,
            final_states = dfa_final_states
        )

        return dfa
