#!/usr/bin/env python3

from abc import ABC, abstractmethod


class FiniteAutomaton(ABC):
    """
    An abstract base class for finite automata.
    """
    def __init__(self, *args):
        super().__init__(args)

    @abstractmethod
    def accepts(self, input_string, trace=False):
        """
        Check if the automaton accepts a string
        Args:
            input_string (str): string consisting of symbols from
                the alphabet.
            trace (bool): print transitions the automaton goes through.

        Should return True if the string is accepted, otherwise False

        """
        pass